let matrixElem;
let arrayElem;


function createMatrix(matrixElem = 2, arrayElem = 3, value = 0) {
	let matrix = [];
	for (i = 0; i < matrixElem; i++) {
		matrix[i] = [];
		for (j = 0; j < arrayElem; j++) {
			matrix[i].push(value++);
		}
	}
	return matrix;
}

let matrix = createMatrix();

function set(matrix, i, j, value) {
	matrix[i][j] = value;
}

//set(matrix, 1,1,0);
/**
 * m(2,2) ->
 *  0 0
 *  0 0
 */
function print(matrix) {
	console.log(` m(${matrix.length}, ${matrix[0].length}) ->`);
	let arrayLine = "";
	let i = 0;
	let j = 0;
	for (i = 0; i < matrix.length; i++) {
		for (j = 0; j < matrix[0].length; j++) {
			arrayLine += `${matrix[i][j]} `;
		}
		arrayLine += "\n";
	}
	return arrayLine;
}

console.log(print(matrix));

function getWidth(matrix) {
	return matrix[0].length;
}
function getHeight(matrix) {
	return matrix.length;
}

function turnLeft(matrix){
	let i = 0;
	let j = 0;
	let matrixSize = matrix.length * matrix[0].length;
	let turnedMatrixLeft = createMatrix(matrix[0].length, matrix.length);
	console.log("Turned left");
	for (i = 0; i < getHeight(matrix); i++) {
		for (j = 0; j < getWidth(matrix); j++) {
			//console.log(i, j, '->', getWidth(matrix) -1 - j , i); 
			//turnedMatrix[j][getHeight(matrix) - 1 - i] = matrix[i][j];
			turnedMatrixLeft[getWidth(matrix) -1 - j][i] =  matrix[i][j];

		}
	}
	
	return turnedMatrixLeft;
}

function turnRight(matrix){
	let i = 0;
	let j = 0;
	let matrixSize = matrix.length * matrix[0].length;
	let turnedMatrixRight = createMatrix(matrix[0].length, matrix.length);
	console.log("Turned right");
	for (i = 0; i < getHeight(matrix); i++) {
		for (j = 0; j < getWidth(matrix); j++) {
			//console.log(i, j, '->', getWidth(matrix) -1 - j , i); 
			turnedMatrixRight[j][getHeight(matrix) - 1 - i] = matrix[i][j];

		}
	}
	
	return turnedMatrixRight;
}


console.log(print(turnRight(matrix)));


let matrixArray;
let arrayElements;

function createMatrixN2(matrixArray = 2, arrayElements = 10, value = 1) {
	let matrixN2 = [];
	for (i = 0; i < matrixArray; i++) {
		matrixN2[i] = [];
		for (j = 0; j < arrayElements; j++) {
			if((j +1) % 4 == 0){
				value = value++;
				matrixN2[i].push(value*value);
			} else{
				matrixN2[i].push(value++);
			}
		}
	}
	return matrixN2;
	console.log(matrixN2);
}

let matrixN2Results = createMatrixN2();

function makeMatrixN2(matrixName) {
	let newMatrixN2 = [];
	let matrixElem = matrixName.length;
	let arrayElem = matrixName[0].length;
	let value;
	for (i = 0; i < matrixElem; i++) {
		newMatrixN2[i] = [];
		for (j = 0; j < arrayElem; j++) {
			value = matrixName[i][j];
			if((j +1) % 4 == 0){
				newMatrixN2[i].push(value*value);
			}else{
				newMatrixN2[i].push(value);
			}
		}
	}
	return newMatrixN2;
}

console.log(matrix);
 

function myFunction(number, modifyFunction) {
	a = number;
	a = modifyFunction(a);
	console.log(a);
}

function sqr(number){
	return number * number;
}

function one(number){
	return number - 1;
}

function print(number){
	console.log (number)
}

myFunction(12, print);
myFunction(12, one);
myFunction(12, sqr);

function matrixForeach(matrix, callbackf){
	for(i = 0; i < matrix.length; i++){
		for(j = 0; j < matrix[0].length; j++){
			callbackf(matrix[i][j], i, j);
		}
	}
	
}

//testFunction(matrix, myf);

//testFunction(matrix);
function myf(value, i, j){ console.log(arguments); }

function test(value, i, j) {
	return (i * getWidth(matrix) + j + 1) % 2 == 0;
}

function makeMatrixN2_v2(matrix) {
	var rez = createMatrix(getHeight(matrix), getWidth(matrix));
	matrixForeach(matrix, function(val, i, j) {
		rez[i][j] = test(val, i, j)? sqr(val) : val;
	});
	return rez;
}

print(matrix);
print(makeMatrixN2_v2(matrix));