const container = document.querySelector('div.container');
let topNum = 10;
let leftNum = 10;
const numOfAlived = 15;
let numberOfIteration = 30;
let index;
let array;
let alivedCells = new Array();
let deadCells = new Array();
let alivedForNextState = [];
let cellsToDelete = [];

function divGenerate(count = 100) {
	let top = 0;
	let left = 0;
	let cellNum = 0;
	for (i = 0; i < topNum; i++) {
		for (j = 0; j < leftNum; j++) {
			let iDiv = document.createElement('div');
			iDiv.className = `cell${cellNum}`;
			iDiv.style.marginLeft = left + "vw";
			iDiv.style.marginTop = top + "vh";
			left += leftNum;
			container.appendChild(iDiv);
			cellNum += 1;
		}
		top += topNum;
		left = 0;
	}

}
divGenerate();

// downloading from DOM
let cells = document.querySelectorAll("div.container>div");
let cellsArray = [...cells];

function drawingForGame(array,cellsToDelete) {
	if (array == 0) {
		randomAlivedCells();
		//test();
	} else {
		for (let i = 0; i < cellsToDelete.length; i++) {
			z = cellsToDelete[i];
			cellsArray[z].classList.remove("black");
		}
		for (let i = 0; i < array.length; i++) {
			y = array[i];
			cellsArray[y].classList.add("black");
		}
	}

}

function randomAlivedCells() {
	for (i = 0; i < numOfAlived; i++) {
		y = Math.floor(Math.random() * Math.floor(100));
		alivedCells.push(y);
		cellsArray[y].classList.add("black");
	}
	return alivedCells;
}
function test() {
	alivedForNextState.push(32);
	cellsArray[32].classList.add("black");
	alivedForNextState.push(42);
	cellsArray[42].classList.add("black");
	alivedForNextState.push(52);
	cellsArray[52].classList.add("black");
	return alivedCells;
}

function getDeadCells() {
	deadCells = [];
	//console.log(deadCells);
	for (i = 0; i < cellsArray.length; i++) {
		if (cellsArray[i].classList.contains("black")) {} else {
			deadCells.push(i);
		}
	}
	return deadCells;
}
//getDeadCells();
function checkNeighborsForAlived(alivedCells) {
	let numOfNeigh;
	let a;
	let alive;
	const c = alivedCells;
	cellsToDelete = [];
	alivedForNextState = [];
	console.log(`alivedCells ${alivedCells}`);
	for (i = 0; i < c.length; i++) {
		a = c[i];
		numOfNeigh = 0;
		if (a > 0 && cellsArray[a - 1].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a > 8 && cellsArray[a - 9].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a > 10 && cellsArray[a - 11].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a > 9 && cellsArray[a - 10].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a < 99 && cellsArray[a + 1].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a < 91 && cellsArray[a + 9].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a < 90 && cellsArray[a + 10].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (a < 89 && cellsArray[a + 11].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (numOfNeigh == 2 || numOfNeigh == 3) {
			alive = alivedCells[i];
			alivedForNextState.push(alive);
		}
		if (numOfNeigh < 2 || numOfNeigh > 3) {
		// 	//cellsArray[a].classList.remove("black");
			 itemToDelete = alivedCells[i];
			 cellsToDelete.push(itemToDelete);

		}
	}
	console.log(`New array with alived cells for next state: ${alivedForNextState}`);
	console.log(`Array with cells, which have to die for next state: ${cellsToDelete}`);
	return alivedForNextState, cellsToDelete;

}

function checkNeighborsForDead(alivedForNextState) {
	let deadCells = getDeadCells();
	for (i = 0; i < deadCells.length; i++) {
		b = deadCells[i];
		numOfNeigh = 0;
		if (b > 0 && cellsArray[b - 1].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b > 8 && cellsArray[b - 9].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b > 10 && cellsArray[b - 11].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b > 9 && cellsArray[b - 10].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b < 99 && cellsArray[b + 1].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b < 91 && cellsArray[b + 9].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b < 90 && cellsArray[b + 10].classList.contains("black")) {
			numOfNeigh += 1;
		}
		if (b < 89 && cellsArray[b + 11].classList.contains("black")) {
			numOfNeigh += 1;
		}
		//console.log(`Number ${b} has ${numOfNeigh} neighbours`)
		if (numOfNeigh == 3) {
			alivedForNextState.push(b);
		}
	}
	console.log(`All items(alived and dead) for next state: ${alivedForNextState}`);
	return alivedForNextState;
}


//console.log(`New array for next state ${alivedC}`)

async function mainMethodGame() {
	//let arrayToGenerate = cellsArray;
	for (let i = 0; i < 30; i++) {
		array =  alivedForNextState;
		console.log(array);
		drawingForGame(array, cellsToDelete);
		checkNeighborsForAlived(array);
		checkNeighborsForDead(alivedForNextState);
		await delay(50);
	}
}

function delay(ms) {
	return new Promise(resolve => setTimeout(resolve, 500));
}

mainMethodGame();